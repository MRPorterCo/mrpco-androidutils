package com.mrporterco.geo;

import com.mrporterco.geo.Constants.GeniusModes;

public interface GeoProperties {

	boolean isGenius();
	GeniusModes getGeniusMode();
	
	long getMinTime();
	double getMinDistance();
}

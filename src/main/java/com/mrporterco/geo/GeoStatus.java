package com.mrporterco.geo;

import java.util.List;

import com.mrporterco.geo.data.beans.SatSignal;


public interface GeoStatus {

	void setSignals(List<SatSignal> signals);

	void setGpsRunning(boolean b);
	
	void setAccuracy(double accuracy);
	
	void setLastLat(double lastLat);
	void setLastLong(double lastLong);
	
	double getAccuracy();

	double getLastLat();

	double getLastLong();

	boolean isOn();

}
